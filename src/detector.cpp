#include <ros/ros.h>
#include <geometry_msgs/PointStamped.h>
// OpenCV
#include <image_transport/image_transport.h>
#include <opencv2/highgui/highgui.hpp>
#include <cv_bridge/cv_bridge.h>
#include <opencv2/features2d.hpp>
#include "opencv2/opencv.hpp"

ros::Publisher pub, pub1;
double maxArea, minArea, minInertiaRatio, maxInertiaRatio;
int minRange, maxRange;
bool SHOWIMAGES;


void image_cb(const sensor_msgs::ImageConstPtr& msg, const sensor_msgs::CameraInfoConstPtr& info_msg)
{
 
	cv_bridge::CvImagePtr cv_ptr;
    	try
    	{
      		cv_ptr = cv_bridge::toCvCopy(msg, sensor_msgs::image_encodings::TYPE_16UC1);
                //cv_ptr = cv_bridge::toCvCopy(msg, sensor_msgs::image_encodings::BGR8);
    	}
    	catch (cv_bridge::Exception& e)
    	{
      		ROS_ERROR("cv_bridge exception: %s", e.what());
      		return;
    	}	
	

	//cv::namedWindow("original");
	//cv::imshow("original", cv_ptr->image);

	cv::Mat processed, img8;

        cv_ptr->image.convertTo(img8, CV_8UC1, 0.1);

	//cv::namedWindow("img8");
	//cv::imshow("img8", img8);

        //cv::threshold(img8, processed, 250, 255, cv::THRESH_BINARY_INV);

	cv::inRange(img8, cv::Scalar(minRange), cv::Scalar(maxRange),processed);


//	cv::namedWindow("processed");
//	cv::imshow("processed", processed);

        cv::erode(processed, processed, cv::Mat(), cv::Point(-1, -1), 5);

	cv::dilate(processed, processed, cv::Mat(), cv::Point(-1, -1), 20);


	cv::SimpleBlobDetector::Params params;
	params.minDistBetweenBlobs = 100.0f;
	params.filterByInertia = true;
	params.minInertiaRatio = minInertiaRatio;
	params.maxInertiaRatio = maxInertiaRatio;
	params.filterByConvexity = false;
	params.filterByColor = false;
	params.filterByCircularity = false;
	params.filterByArea = true;
	params.minArea = minArea;
	params.maxArea = maxArea;


	cv::Ptr<cv::SimpleBlobDetector> blob_detector = cv::SimpleBlobDetector::create(params);

        std::vector<cv::KeyPoint> keypoints;
	blob_detector->detect(processed, keypoints);

	cv::Mat im_with_keypoints;

	if (SHOWIMAGES)
		cv::drawKeypoints(processed, keypoints, im_with_keypoints, cv::Scalar(0, 0, 255), cv::DrawMatchesFlags::DRAW_RICH_KEYPOINTS);

        //for (int i=0; i<keypoints.size();i++)
	//	ROS_INFO("%f %f %f", keypoints[i].size, keypoints[i].pt.x, keypoints[i].pt.y);
	//ROS_INFO("___"); 

	geometry_msgs::PointStamped center;
	center.point.x=-1.0;
	center.point.y=-1.0;
	center.point.z=-1.0;
	center.header = msg->header;

	if (keypoints.size()>0){
		int i=keypoints.size()-1;
		cv::circle(im_with_keypoints, keypoints[i].pt, 50, cv::Scalar( 0, 0, 255 ), cv::FILLED, cv::LINE_8 );
                cv::Point pt1, pt2;
		float side = keypoints[i].size/10.0;
		pt1.x=keypoints[i].pt.x-side;
		if (pt1.x<0.0) pt1.x=0;
		pt1.y=keypoints[i].pt.y+side;
		if (pt1.y>=processed.rows) pt1.y=processed.rows-1;
		pt2.x=keypoints[i].pt.x+side;
		if (pt2.x>=processed.cols) pt2.x=processed.cols-1;
		pt2.y=keypoints[i].pt.y-side;
		if (pt2.y<0.0) pt2.y=0;

//		cv::rectangle(im_with_keypoints, pt1, pt2, cv::Scalar( 0, 0, 255 ), cv::FILLED, cv::LINE_8 );

		cv::Rect roi(pt1, pt2);
		cv::Mat image_roi = cv_ptr->image(roi);
		cv::Scalar depth = cv::mean(image_roi); 
		center.point.x = depth.val[0]*((keypoints[i].pt.x - info_msg->K[2]) / info_msg->K[0]);
    		center.point.y = depth.val[0]*((keypoints[i].pt.y - info_msg->K[5]) / info_msg->K[4]);
		center.point.z = depth.val[0];

		ROS_INFO("%f %f %f", center.point.x, center.point.y, center.point.z);
	}
	else
		ROS_INFO("No detection!");	
	
	pub1.publish(center);

	if (SHOWIMAGES)
	{
        	cv::namedWindow("im_with_keypoints");
		cv::imshow("im_with_keypoints", im_with_keypoints);
		cv::waitKey(1);      
	}

}

int
main (int argc, char** argv)
{
  // Initialize ROS
  ros::init (argc, argv, "tree_detector");

  ros::NodeHandle nh("~");

  nh.param("maxArea", maxArea, 8000000.0);
  nh.param("minArea", minArea, 10000.0);	
  nh.param("maxInertiaRatio", maxInertiaRatio, 0.4);
  nh.param("minInertiaRatio", minInertiaRatio, 0.0);
  nh.param("maxRange", maxRange, 60);
  nh.param("minRange", minRange, 25);
  nh.param("showImages", SHOWIMAGES, true);		 

  //ROS_INFO("%f %f %f %f %d %d", maxArea, minArea, maxInertiaRatio, minInertiaRatio, maxRange, minRange );

  image_transport::ImageTransport it(nh);
  	
  // Create a ROS subscriber for the input image
  image_transport::CameraSubscriber sub1 = it.subscribeCamera("/l515/depth/image_rect_raw", 1, image_cb);
  
  // Create a ROS publisher for the center of the tree
  pub1 = nh.advertise<geometry_msgs::PointStamped> ("tree", 1);	

  // Spin
  ros::spin ();
}
