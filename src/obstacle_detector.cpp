#include <ros/ros.h>
#include <geometry_msgs/PointStamped.h>
// PCL specific includes
#include <sensor_msgs/PointCloud2.h>
#include <sensor_msgs/PointCloud.h>
#include <pcl_conversions/pcl_conversions.h>
#include <pcl/point_cloud.h>
#include <pcl/point_types.h>
#include <pcl/filters/voxel_grid.h>
#include <pcl/kdtree/kdtree.h>
#include <pcl/segmentation/extract_clusters.h>
#include <pcl/common/centroid.h>
// OpenCV
#include <image_transport/image_transport.h>
#include <opencv2/highgui/highgui.hpp>
#include <cv_bridge/cv_bridge.h>
#include <opencv2/features2d.hpp>
#include "opencv2/opencv.hpp"


ros::Publisher pub, pub1;
int minRange, maxRange;
bool SHOWIMAGES;

void 
cloud_cb (const sensor_msgs::PointCloud2ConstPtr& cloud_msg)
{
  //ROS_INFO("%s %s %s %s", cloud_msg->fields[0].name.c_str(), cloud_msg->fields[1].name.c_str(), 
  //		cloud_msg->fields[2].name.c_str(), cloud_msg->fields[3].name.c_str());
  
  if(cloud_msg->data.size() > 0){
  	// Convert from ROS to PCL data type
  	pcl::PointCloud<pcl::PointXYZRGB>::Ptr cloudRGB (new pcl::PointCloud<pcl::PointXYZRGB>);
  	pcl::fromROSMsg (*cloud_msg, *cloudRGB); // The original point cloud has color information

  	pcl::PointCloud<pcl::PointXYZ>::Ptr cloud (new pcl::PointCloud<pcl::PointXYZ>); // Only XYZ
  	pcl::copyPointCloud(*cloudRGB, *cloud); 
 
  	// Perform the actual clustering
  	pcl::search::KdTree<pcl::PointXYZ>::Ptr tree (new pcl::search::KdTree<pcl::PointXYZ> ());
  	tree->setInputCloud(cloud);
  	std::vector<pcl::PointIndices> cluster_indices;
  	pcl::EuclideanClusterExtraction<pcl::PointXYZ> ec;
  	ec.setSearchMethod(tree);	
  	ec.setInputCloud(cloud);
  	ec.setClusterTolerance (0.0005);
  	ec.setMinClusterSize (300);
  	ec.setMaxClusterSize (1000000);
  	ec.extract (cluster_indices); // Does the work

  	ROS_INFO("Number of clusters: %d", (int)cluster_indices.size());

	sensor_msgs::PointCloud output;
  	
	// iteract over all clusters
  	for (std::vector<pcl::PointIndices>::const_iterator it = cluster_indices.begin (); it != cluster_indices.end (); ++it){ 
  
        	// each cluster represented by it
        	pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_cluster (new pcl::PointCloud<pcl::PointXYZ>);
  		for (std::vector<int>::const_iterator pit = it->indices.begin(); pit != it->indices.end (); pit++){
        		cloud_cluster->points.push_back(cloud->points[*pit]);
  		}
  		cloud_cluster->width = int (cloud_cluster->points.size ());
  		cloud_cluster->height = 1;
  		cloud_cluster->is_dense = true;

		// Find the center of the cluster (X, Y, Z)

		Eigen::Vector4f centroid; 
		pcl::compute3DCentroid (*cloud_cluster, centroid); 
		//ROS_INFO("centroid: (%f, %f, %f)", centroid[0], centroid[1], centroid[2]);

		// Based on the distance decides if the cluster is an obstacle 
		float distance = sqrt(centroid[0]*centroid[0]+centroid[1]*centroid[1]+centroid[2]*centroid[2]);
  	
		//ROS_INFO("centroid: (%f, %f, %f)", centroid[0], centroid[1], centroid[2]);

		if (distance<0.5){ 	

			//ROS_INFO("distance: %f", distance);

  			// Convert to ROS data type
  			//pcl::toROSMsg(*cloud_cluster, output);
			geometry_msgs::Point32 center;
			center.x=centroid[0];
			center.y=centroid[1];
			center.z=centroid[2];
			output.points.push_back(center);
  			
		}
		
  	} // for
	output.header=cloud_msg->header;
	// Publish the data
	pub.publish (output);
	ROS_INFO("Close clusters %d", (int)output.points.size());
  } // if 
   else
      ROS_WARN("No data!");
}

void image_cb(const sensor_msgs::ImageConstPtr& msg, const sensor_msgs::CameraInfoConstPtr& info_msg)
{
 
	cv_bridge::CvImagePtr cv_ptr;
    	try
    	{
      		cv_ptr = cv_bridge::toCvCopy(msg, sensor_msgs::image_encodings::TYPE_16UC1);
    	}
    	catch (cv_bridge::Exception& e)
    	{
      		ROS_ERROR("cv_bridge exception: %s", e.what());
      		return;
    	}	
	

	//cv::namedWindow("original");
	//cv::imshow("original", cv_ptr->image);

	cv::Mat processed, img8;

        cv_ptr->image.convertTo(img8, CV_8UC1, 0.1);

	if (SHOWIMAGES)
	{
		cv::namedWindow("Obstacle-img8");
		cv::imshow("Obstacle-img8", img8);
	}
        
	cv::inRange(img8, cv::Scalar(minRange), cv::Scalar(maxRange), processed);

        cv::erode(processed, processed, cv::Mat(), cv::Point(-1, -1), 5);

	if (SHOWIMAGES)
	{
		cv::namedWindow("Obstacle-processed");
		cv::imshow("Obstacle-processed", processed);
	}
	sensor_msgs::PointCloud output;

	for (int i = 0; i < processed.cols; i++) {
    		for (int j = 0; j < processed.rows; j++) {
        		if (processed.at<uchar>(j, i)){
				geometry_msgs::Point32 point;
				point.x = cv_ptr->image.at<unsigned short>(j, i)*((j - info_msg->K[2]) / info_msg->K[0]);
    				point.y = cv_ptr->image.at<unsigned short>(j, i)*((i - info_msg->K[5]) / info_msg->K[4]);
				point.z = cv_ptr->image.at<unsigned short>(j, i);
				output.points.push_back(point);
			}	
    		}
	}	
	output.header=msg->header;
	// Publish the data
	pub.publish (output);
	ROS_INFO("Number of points %d", (int)output.points.size());


	if (SHOWIMAGES)
	{
		cv::waitKey(1);      
	}
}


int
main (int argc, char** argv)
{
  // Initialize ROS
  ros::init (argc, argv, "obstacle_detector");

  ros::NodeHandle nh("~");

  nh.param("maxRange", maxRange, 60);
  nh.param("minRange", minRange, 25);
  nh.param("showImages", SHOWIMAGES, true);		

  // Create a ROS subscriber for the input point cloud
  //ros::Subscriber sub = nh.subscribe ("/l515/depth/color/points", 1, cloud_cb);

  image_transport::ImageTransport it(nh);
  	
  // Create a ROS subscriber for the input image
  image_transport::CameraSubscriber sub1 = it.subscribeCamera("/l515/depth/image_rect_raw", 1, image_cb);
  

  // Create a ROS publisher for the output point cloud
  pub = nh.advertise<sensor_msgs::PointCloud> ("output", 1);

  
  // Spin
  ros::spin ();
}
